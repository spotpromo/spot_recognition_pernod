package br.com.spotpromo.spotrecognition.model

import android.graphics.Bitmap
import br.com.spotpromo.spotrecognition.tflite.Classifier
import br.com.spotpromo.spotrecognition.utils.MultBoxTracker

class DataRecognition {
    var path: String? = null
    var recognition: List<MultBoxTracker.TrackedRecognition>? = null
}