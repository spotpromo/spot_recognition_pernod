package br.com.spotpromo.spotrecognition

import android.R.attr
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.spotpromo.spotrecognition.env.Utils
import br.com.spotpromo.spotrecognition.model.DataRecognition
import br.com.spotpromo.spotrecognition.tflite.Classifier
import br.com.spotpromo.spotrecognition.tflite.DetectorFactory
import br.com.spotpromo.spotrecognition.utils.CheckReadPermission
import br.com.spotpromo.spotrecognition.utils.MultBoxTracker
import br.com.spotpromo.spotrecognition.utils.UtilsRI
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.spotpromo.spotcamerarecognitionnovo.utils.Alerta
import com.spotpromo.spotcamerarecognitionnovo.utils.ExifUtil
import kotlinx.android.synthetic.main.layout_fragmento_ri_teste.*
import okhttp3.internal.Util
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class SpotRecognitionTest : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {

    }
//    private var detector: Classifier? = null
//
//    protected var previewWidth = 0
//    protected var previewHeight = 0
//    lateinit var spotoRecognition: SpotRecognition
//    val TF_OD_API_INPUT_SIZE = 960
//
//    val listafotos = ArrayList<String>()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.layout_fragmento_ri_teste)
//        try {
//            spotoRecognition = SpotRecognition()
//
//            controles()
//        } catch (err: Exception) {
//
//        }
//    }
//
//
//    private fun controles() {
//        toolbar.title = "SPOT RI"
//
//        spotoRecognition.initBox(this)
//
//        fabfoto.setOnClickListener(this)
//        fabanalizar.setOnClickListener(this)
//        if (!CheckReadPermission.validaPermissao(this)) {
//            if (Build.VERSION.SDK_INT >= 23)
//                CheckReadPermission.show(this@SpotRecognitionTest)
//        }
//
//        val modelStrings = UtilsRI.getModelStrings(
//            assets,
//            UtilsRI.ASSETS_PATH
//        )
//        Log.e("ANDROID_ASSETS", modelStrings[0])
//        if (detector == null)
//            detector = DetectorFactory.getDetector(assets, modelStrings[0], TF_OD_API_INPUT_SIZE)
//
//    }
//
//    override fun onClick(v: View?) {
//        try {
//            when (v) {
//                fabfoto -> {
//                    UtilsRI.abrirGaleriaCameraRecognition(this@SpotRecognitionTest, 1, 1)
//
////                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
////                    if (takePictureIntent.resolveActivity(packageManager) != null) {
////                        startActivityForResult(takePictureIntent, 1)
////                    }
//                }
//                fabanalizar -> {
//
//                    if (listafotos.isEmpty()) {
//                        Alerta.show(
//                            this@SpotRecognitionTest,
//                            "Atenção",
//                            "Captura a(s) foto(s).",
//                            false
//                        )
//                        return
//                    }
//
//                    val foto = listafotos.first()
////
////                    doAsync {
////
////                        activityUiThread {
//
//
//                            //val newFile = onDimensionPicture(this@SpotRecognitionTest, File(foto))
//                            val bitmap: Bitmap = BitmapFactory.decodeFile(foto)
//                            previewWidth = bitmap.width
//                            previewHeight = bitmap.height;
//                            val cropImage: Bitmap? =
//                                UtilsRI.processBitmap(bitmap, TF_OD_API_INPUT_SIZE)
//
//
//
//                            Log.e(
//                                "RECOGNITION",
//                                "=================================================================>>  IMAGE CROPPED"
//                            )
//                            val recognition = detector!!.recognizeImage(cropImage)
//                            val result = handleResult(
//                                this@SpotRecognitionTest,
//                                foto,
//                                cropImage!!,
//                                recognition
//                            )
//
//                            Log.e(
//                                "RECOGNITION",
//                                "=================================================================>>  RECOGNITION OK " + result
//                            )
//
//                        }
//                    }
////                }
////            }
//
//        } catch (err: java.lang.Exception) {
//            Log.e("ERROR", err.toString())
//        }
//    }
//
//    private fun onDimensionPicture(context: Activity, file: File): String {
//
//        try {
//            val options = RequestOptions()
//                .override(TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//
//
//            val bitmap: Bitmap = Glide.with(context)
//                .asBitmap()
//                .load(file)
//                .apply(options)
//                .submit()
//                .get()
//
//            val timeStamp = String.format(
//                "%s_%s_%s", "Foto_RI", SimpleDateFormat("yyyyMMdd_HHmmss.SSS", Locale.getDefault())
//                    .format(Date()), ".jpg"
//            )
//
//            Log.e(
//                "RECOGNITION",
//                "=================================================================>>  NEW FILE $timeStamp"
//            )
//
//            val outputFile = createFileResize(context, timeStamp)
//
//            saveBitmapToFile(bitmap, outputFile)
//
//
//            return outputFile.absolutePath
//        } catch (e: Exception) {
//            Alerta.show(this@SpotRecognitionTest, "Atenção", e.message!!, false)
//        }
//
//        return ""
//
//    }
//
//    @Throws(IOException::class)
//    private fun createFileResize(context: Context, name: String): File {
//
//        val sdcard = context.getExternalFilesDir(null)
//        val pastaProjeto = File(sdcard!!.absolutePath + File.separator + "SPOT_RI")
//
//        if (!pastaProjeto.isDirectory) pastaProjeto.mkdirs()
//
//        val newFile = File(pastaProjeto.absolutePath + File.separator + name)
//        newFile.createNewFile()
//
//
//        return newFile
//    }
//
//
//    private fun saveBitmapToFile(bitmap: Bitmap, outputFile: File) {
//        try {
//            FileOutputStream(outputFile).use { fos ->
//                bitmap.compress(
//                    Bitmap.CompressFormat.PNG,
//                    100,
//                    fos
//                )
//            }
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//    }
//
//    private fun handleResult(
//        context: Activity,
//        path: String,
//        bitmap: Bitmap,
//        results: List<Classifier.Recognition>
//    ): String {
//
//        try {
//            if (!results.isNullOrEmpty()) {
//                val canvas = Canvas(bitmap)
//
//                MultBoxTracker.proccess(
//                    context,
//                    results,
//                    canvas,
//                    UtilsRI.getRotationFile(path),
//                    bitmap.width,
//                    bitmap.height
//                )
//                val resizeBitmap = UtilsRI.getReszeBitmap(
//                    context,
//                    bitmap,
//                    previewWidth,
//                    previewHeight
//                )
//                val data = DataRecognition()
//                data.path = resizeBitmap
//                data.recognition = MultBoxTracker.getDataTrackedObjects()
//                return Gson().toJson(data)
//            }
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        return ""
//
//    }
//
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        try {
//
//
//
//            if (resultCode == RESULT_OK && requestCode == 1) {
//
//                val lista_arquivos = data!!.extras!!.getSerializable("arquivo") as ArrayList<String>
//                if(!lista_arquivos.isNullOrEmpty()) {
//                    val file = File(lista_arquivos.first())
//
//                    if (file.isFile) {
//
//                        val mFotoNova = UtilsRI.retornaCaminhoFotoURINew(
//                            Uri.fromFile(File(file.absolutePath)),
//                            99999,
//                            "Foto_RI",
//                            this
//                        )!!
//
//                        file.delete()
//
//                        listafotos.add(mFotoNova)
//
//                        UtilsRI.mostraImagem(img_foto, listafotos.first())
//                    }
//                }
//
////                val extras: Bundle? = data!!.getExtras()
////                val imageBitmap = extras!!["data"] as Bitmap?
////
////
////                img_foto.setImageBitmap(imageBitmap)
////              val fileRename =  UtilsRI.bitmapToFile(
////                    this@SpotRecognitionTest,
////                    imageBitmap!!,
////                    "Foto_RI_Temp.jpg"
////                )!!.absolutePath
////
////                listafotos.add(
////
////                )
////
////                UtilsRI.mostraImagem(img_foto, listafotos.first())
//            }
//
//        } catch (err: java.lang.Exception) {
//            Log.e("ERROR", err.toString())
//        }
//    }
//
//    private fun getLabels(): ArrayList<String> {
//        val labels = ArrayList<String>()
//        val file_name = "coco.txt"
//        application.assets.open(file_name).bufferedReader().useLines {
//            it.forEach {
//                labels.add(it)
//                Log.e("LABELS", it.toString())
//            }
//        }
//
//        return labels
//    }


}