package br.com.spotpromo.spotrecognition.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.spotpromo.spotcamerarecognitionnovo.SpotCameraRecognition
import com.spotpromo.spotcamerarecognitionnovo.config.SpotCameraRecognitionConfig
import com.spotpromo.spotcamerarecognitionnovo.gallery.SpotGalleryRecognition
import java.io.*
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.text.SimpleDateFormat
import java.util.*


object UtilsRI {
    var ASSETS_PATH = ""
    fun getModelStrings(mgr: AssetManager, path: String?): ArrayList<String> {
        val res = ArrayList<String>()
        try {
            val files = mgr.list(path!!)
            for (file in files!!) {
                val splits = file.split("\\.".toRegex()).toTypedArray()
                if (splits[splits.size - 1] == "tflite") {
                    res.add(file)
                }
            }
        } catch (e: IOException) {
            System.err.println("getModelStrings: " + e.message)
        }

        return res
    }

    /**
     * Memory-map the model file in Assets.
     */
    @Throws(IOException::class)
    fun loadModelFile(assets: AssetManager, modelFilename: String): MappedByteBuffer {
        val fileDescriptor = assets.openFd(modelFilename)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength

        Log.e(
            "TENSORFLOWWWW",
            "------------------------------> MODEL  " + fileChannel.map(
                FileChannel.MapMode.READ_ONLY,
                startOffset,
                declaredLength
            )
        );
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    fun processBitmap(source: Bitmap, size: Int): Bitmap? {
        val image_height = source.height
        val image_width = source.width
        val croppedBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
        val frameToCropTransformations =
            getTransformationMatrix(image_width, image_height, size, size, 0, false)
        val cropToFrameTransformations = Matrix()
        frameToCropTransformations!!.invert(cropToFrameTransformations)
        val canvas = Canvas(croppedBitmap)
        canvas.drawBitmap(source, frameToCropTransformations, null)


        return croppedBitmap
    }

    fun getTransformationMatrix(
        srcWidth: Int,
        srcHeight: Int,
        dstWidth: Int,
        dstHeight: Int,
        applyRotation: Int,
        maintainAspectRatio: Boolean
    ): Matrix? {
        val matrix = Matrix()
        if (applyRotation != 0) {
            // Translate so center of image is at origin.
            matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f)

            // Rotate around origin.
            matrix.postRotate(applyRotation.toFloat())
        }

        // Account for the already applied rotation, if any, and then determine how
        // much scaling is needed for each axis.
        val transpose = (Math.abs(applyRotation) + 90) % 180 == 0
        val inWidth = if (transpose) srcHeight else srcWidth
        val inHeight = if (transpose) srcWidth else srcHeight

        // Apply scaling if necessary.
        if (inWidth != dstWidth || inHeight != dstHeight) {
            val scaleFactorX = dstWidth / inWidth.toFloat()
            val scaleFactorY = dstHeight / inHeight.toFloat()
            if (maintainAspectRatio) {
                // Scale by minimum factor so that dst is filled completely while
                // maintaining the aspect ratio. Some image may fall off the edge.
                val scaleFactor = Math.max(scaleFactorX, scaleFactorY)
                matrix.postScale(scaleFactor, scaleFactor)
            } else {
                // Scale exactly to fill dst from src.
                matrix.postScale(scaleFactorX, scaleFactorY)
            }
        }
        if (applyRotation != 0) {
            // Translate back from origin centered reference to destination frame.
            matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f)
        }
        return matrix
    }


    fun getRotationFile(path: String): Int {
        var rotate = 0
        try {
            val file = File(path)
            if (file.isFile) {
                val exif = ExifInterface(file.absolutePath)
                val orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL
                )
                rotate = when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_270 -> 270
                    ExifInterface.ORIENTATION_ROTATE_180 -> 180
                    ExifInterface.ORIENTATION_ROTATE_90 -> 90
                    else -> 0
                }


            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return rotate
    }

    fun getReszeBitmap(context: Context, bm: Bitmap, width: Int, height: Int): String {
        var bmWidth = bm.width
        var bmHeight = bm.height

        val scaleWidth = width.toFloat() / bmWidth
        val scaleHeight = height.toFloat() / bmHeight

        val croppedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val frameToCropTransformations: Matrix =
            getTransformationMatrix(
                bmWidth,
                bmHeight,
                width,
                height,
                0,
                false
            )!!
        val cropToFrameTransformations = Matrix()
        frameToCropTransformations.invert(cropToFrameTransformations)

        val canvas = Canvas(croppedBitmap)
        canvas.drawBitmap(bm, frameToCropTransformations, null)

        val out = ByteArrayOutputStream()
        croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(out.toByteArray()))

        return saveInStorage(context, decoded)
    }

    fun saveInStorage(context: Context, bm: Bitmap): String {

        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + "PERNOD"
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + "SPOTPROMO"
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()

        val string_file_copy: String = CriarFileTemporarioString(
            pasta_spot.absolutePath,
            "temp_recognition"
        )

        val file_copy = File(string_file_copy)
        val out = FileOutputStream(file_copy)
        bm.compress(Bitmap.CompressFormat.PNG, 100, out)

        return file_copy.absolutePath

    }

    fun CriarFileTemporarioString(
        diretorio: String,
        nomefoto: String
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }


    fun View.isvisible(visible: Boolean) {
        visibility = if (visible) View.VISIBLE else View.GONE
    }

    fun View.getvisible(): Boolean = visibility == View.VISIBLE
    fun ImageView.drawable(context: Context, drawable: Int) = setImageDrawable(
        ContextCompat.getDrawable(
            context,
            drawable
        )
    )


    fun abrirGaleriaCameraRecognition(activity: Activity, retorno: Int?, type: Int) {

        try {
            val tamanho = "960x960"

            SpotCameraRecognitionConfig.buttonColor = "#00437C"
            SpotCameraRecognitionConfig.photoSize = tamanho
            SpotCameraRecognitionConfig.isWithGallery = false
            SpotCameraRecognitionConfig.tempFilePath =
                activity.getExternalFilesDir(null).toString() + "/temp_file.jpeg"


            activity.startActivityForResult(
                Intent(
                    activity,
                    if (type == 1) SpotCameraRecognition::class.java else SpotGalleryRecognition::class.java
                ).putExtra("tipo", retorno), retorno!!
            )
        } catch (e: Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }

    }

    fun bitmapToFile(context: Context, bitmap: Bitmap, fileName: String?): File? {
        val file = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName)
        try {
            FileOutputStream(file).use { os ->
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
                return file
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    fun mostraImagem(campoFoto: ImageView, caminhoFoto: String?) {
        if (caminhoFoto != null) {
            val file = File(caminhoFoto)
            if (file != null && file.isFile) {
                val bitmap =
                    SupportImagem.ajustOrientation(
                        file,
                        640,
                        480
                    )

                campoFoto.setImageBitmap(bitmap)

                campoFoto.tag = caminhoFoto
            }
        }
    }

//    fun rotateImageFile(context: Context, imageFile: File): String? {
//        try {
//            val exif = ExifInterface(imageFile.absolutePath)
//            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
//
//            val originalBitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
//            val rotatedBitmap: Bitmap
//
//            rotatedBitmap = when (orientation) {
//                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(originalBitmap, 90f)
//                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(originalBitmap, 180f)
//                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(originalBitmap, 270f)
//                ExifInterface.ORIENTATION_NORMAL, ExifInterface.ORIENTATION_UNDEFINED -> originalBitmap
//                else -> originalBitmap
//            }
//
//            val outputStream = FileOutputStream(imageFile)
//            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
//            outputStream.flush()
//            outputStream.close()
//
//            return imageFile.absolutePath
//        } catch (e: IOException) {
//            e.printStackTrace()
//            return null
//        }
//    }

    fun retornaCaminhoFotoURINew(
        uriAtual: Uri?,
        codRoteiro: Int,
        nomefoto: String?,
        context: Context
    ): String? {
        var retorna_novo_caminho_ou_antigo = ""
        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + "SPOT_RI"
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()


        val stringi_file_copy: String = CriarFileTemporarioStringRoteiro(
            pasta_projeto.absolutePath,
            nomefoto!!,
            codRoteiro
        )
        val file_copy = File(stringi_file_copy)

        /** COPIAR ARQUIVO PARA NOVA PASTA  */
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            `in` = context.contentResolver.openInputStream(uriAtual!!)
            out = FileOutputStream(file_copy)

            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }

            if (file_copy.isFile) retorna_novo_caminho_ou_antigo = file_copy.absolutePath
        } finally {
            `in`?.close()
            out?.close()
        }

        val file_movido = File(retorna_novo_caminho_ou_antigo)
        if (!file_movido.isFile)
            throw Exception("A foto não foi atualizada, por favor tente novamente")

        return retorna_novo_caminho_ou_antigo

    }

    fun CriarFileTemporarioStringRoteiro(
        diretorio: String,
        nomefoto: String,
        codRoteiro: Int
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + codRoteiro + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }
}