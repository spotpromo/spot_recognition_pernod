package br.com.spotpromo.spotrecognition

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.spotpromo.spotrecognition.model.DataRecognition
import br.com.spotpromo.spotrecognition.tflite.Classifier
import br.com.spotpromo.spotrecognition.tflite.DetectorFactory
import br.com.spotpromo.spotrecognition.utils.MultBoxTracker
import br.com.spotpromo.spotrecognition.utils.UtilsRI
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class SpotRecognition: AppCompatActivity() {

    protected var previewWidth = 0
    protected var previewHeight = 0

    val TF_OD_API_INPUT_SIZE = 960

    private var detector: Classifier? = null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        //setContentView(R.layout.activity_main)
//        initBox(this)
//    }

    fun initBox(context: Activity) {
        try {

            val modelStrings = UtilsRI.getModelStrings(
                context.assets,
                UtilsRI.ASSETS_PATH
            )
            Log.e("ANDROID_ASSETS", modelStrings[0])
            if (detector == null)
                detector = DetectorFactory.getDetector(context.assets, modelStrings[0], TF_OD_API_INPUT_SIZE)

        } catch (e: IOException) {
            e.printStackTrace()

            val toast = Toast.makeText(
                context, "Classifier could not be initialized", Toast.LENGTH_SHORT
            )
            toast.show()
        }
    }


    fun onRecognition(context: Activity, path: String): String {

        var result = ""
//        doAsync {
//
//            activityUiThread {


                try {
                    val file = File(path)

                    if (file.isFile) {

                       // val newFile = onDimensionPicture(context, file)
                        val bitmap: Bitmap = BitmapFactory.decodeFile(path)
                        previewWidth = bitmap.width
                        previewHeight = bitmap.height


                        Log.e("RECOGNITION","=================================================================>>  IMAGE SIZE WIDTH: ${previewWidth} / HEIGHT: ${previewHeight}")

                        val cropImage: Bitmap? = UtilsRI.processBitmap(bitmap, TF_OD_API_INPUT_SIZE)


                        Log.e("RECOGNITION","=================================================================>>  IMAGE CROPPED NOVO 2")
                        val recognition = detector!!.recognizeImage(cropImage)
                        Log.e("RECOGNITION","=================================================================>>  RECOGNITION OK NOVO 2 ")
                        result = handleResult(context, path, cropImage!!, recognition)


                        Log.e("RECOGNITION","=================================================================>>  RESULT LENGTH NOVO 2 " + result.length)
                    }

                } catch (err: Exception) {

                    Log.e("RECOGNITION","=================================================================>>  ERROR 2" + err.printStackTrace())
                    err.printStackTrace()
                }
//            }
//        }

        return result
    }

    private fun onDimensionPicture(context: Activity, file: File): String {

        val options = RequestOptions()
            .override(TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE)
            .diskCacheStrategy(DiskCacheStrategy.ALL)


        Log.e("RECOGNITION",
            "=================================================================>>  GLIDE"
        )
        val bitmap: Bitmap = Glide.with(context)
            .asBitmap()
            .load(file)
            .apply(options)
            .submit()
            .get()

        val timeStamp = String.format("%s_%s_%s", "Foto_RI", SimpleDateFormat("yyyyMMdd_HHmmss.SSS", Locale.getDefault())
            .format(Date()), ".jpg")

        Log.e("RECOGNITION",
            "=================================================================>>  NEW FILE $timeStamp"
        )

        val outputFile = createFileResize(context, timeStamp)

        Log.e("RECOGNITION",
            "=================================================================>>  NEW FILE CREATE ${outputFile.absolutePath}"
        )


        saveBitmapToFile(bitmap, outputFile)

        return outputFile.absolutePath
    }

    @Throws(IOException::class)
    private fun createFileResize(context: Context, name: String): File {

        val sdcard = context.getExternalFilesDir(null)
        val pastaProjeto = File(sdcard!!.absolutePath + File.separator + "SPOT_RI")

        if (!pastaProjeto.isDirectory) pastaProjeto.mkdirs()

        val newFile = File(pastaProjeto.absolutePath + File.separator + name)
        newFile.createNewFile()


        return newFile
    }


    private fun saveBitmapToFile(bitmap: Bitmap, outputFile: File) {
        try {
            FileOutputStream(outputFile).use { fos ->
                bitmap.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    fos
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun handleResult(
        context: Activity,
        path: String,
        bitmap: Bitmap,
        results: List<Classifier.Recognition>
    ): String {

        try {
            if (!results.isNullOrEmpty()) {
                val canvas = Canvas(bitmap)

                MultBoxTracker.proccess(
                    context,
                    results,
                    canvas,
                    UtilsRI.getRotationFile(path),
                    bitmap.width,
                    bitmap.height
                )
                val resizeBitmap = UtilsRI.getReszeBitmap(
                    context,
                    bitmap,
                    previewWidth,
                    previewHeight
                )
                val data = DataRecognition()
                data.path = resizeBitmap
                data.recognition = MultBoxTracker.getDataTrackedObjects()
                return Gson().toJson(data)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""

    }
}