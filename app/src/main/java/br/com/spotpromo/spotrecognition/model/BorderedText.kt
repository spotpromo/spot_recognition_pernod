package br.com.spotpromo.spotrecognition.model

import android.graphics.*
import android.graphics.Paint.Align
import java.util.*

class BorderedText {
    private var interiorPaint: Paint? = null
    private var exteriorPaint: Paint? = null
    private var textSize: Float? = null

    constructor(textSize: Float) {
        this(Color.WHITE, Color.BLACK, textSize)
    }

    private operator fun invoke(interiorColor: Int, exteriorColor: Int, textSize: Float) {
        interiorPaint = Paint()
        interiorPaint!!.textSize = textSize
        interiorPaint!!.color = interiorColor
        interiorPaint!!.style = Paint.Style.FILL
        interiorPaint!!.isAntiAlias = false
        interiorPaint!!.alpha = 255

        exteriorPaint = Paint()
        exteriorPaint!!.setTextSize(textSize)
        exteriorPaint!!.setColor(exteriorColor)
        exteriorPaint!!.setStyle(Paint.Style.FILL_AND_STROKE)
        exteriorPaint!!.setStrokeWidth(textSize / 8)
        exteriorPaint!!.setAntiAlias(false)
        exteriorPaint!!.setAlpha(255)

        this.textSize = textSize
    }

    fun setTypeface(typeface: Typeface?) {
        interiorPaint!!.typeface = typeface
        exteriorPaint!!.typeface = typeface
    }

    fun drawText(canvas: Canvas, posX: Float, posY: Float, text: String?) {
        canvas.drawText(text!!, posX, posY, exteriorPaint!!)
        canvas.drawText(text, posX, posY, interiorPaint!!)
    }

    fun drawText(
        canvas: Canvas, posX: Float, posY: Float, text: String?, bgPaint: Paint?
    ) {
        val width = exteriorPaint!!.measureText(text)
        val textSize = exteriorPaint!!.textSize
        val paint = Paint(bgPaint)
        paint.style = Paint.Style.FILL
        paint.alpha = 160
        canvas.drawRect(posX, posY + textSize.toInt(), posX + width.toInt(), posY, paint)
        canvas.drawText(text!!, posX, posY + textSize, interiorPaint!!)
    }

    fun drawLines(canvas: Canvas?, posX: Float, posY: Float, lines: Vector<String?>) {
        var lineNum = 0
        for (line in lines) {
            drawText(canvas!!, posX, posY - getTextSize() * (lines.size - lineNum - 1), line)
            ++lineNum
        }
    }

    fun setInteriorColor(color: Int) {
        interiorPaint!!.color = color
    }

    fun setExteriorColor(color: Int) {
        exteriorPaint!!.color = color
    }

    fun getTextSize(): Float = textSize!!

    fun setAlpha(alpha: Int) {
        interiorPaint!!.alpha = alpha
        exteriorPaint!!.alpha = alpha
    }

    fun getTextBounds(
        line: String?, index: Int, count: Int, lineBounds: Rect?
    ) {
        interiorPaint!!.getTextBounds(line, index, count, lineBounds)
    }

    fun setTextAlign(align: Align?) {
        interiorPaint!!.textAlign = align
        exteriorPaint!!.textAlign = align
    }
}