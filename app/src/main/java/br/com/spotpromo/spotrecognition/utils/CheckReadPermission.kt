package br.com.spotpromo.spotrecognition.utils

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import br.com.spotpromo.spotrecognition.R
import java.util.*


object CheckReadPermission {
    var status = true
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124

    fun show(activity: AppCompatActivity) {

        try {
            val permissionsNeeded = ArrayList<String>()

            val permissionsList = ArrayList<String>()
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_sd_card_escrita))
            if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_sd_card_leitura))
            if (!addPermission(permissionsList, Manifest.permission.CAMERA, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_camera))

            if (permissionsList.size > 0) {
                if (permissionsNeeded.size > 0) {
                    // Need Rationale
                    val sbPermissao = StringBuilder()
                    sbPermissao.append(activity.resources.getString(R.string.msg_permissao))

                    for (i in permissionsNeeded.indices)
                        sbPermissao.append(String.format("<br>%s", permissionsNeeded[i]))

                    if (Build.VERSION.SDK_INT >= 23) {
                        activity.requestPermissions(
                            permissionsList.toTypedArray(),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                        )
                    }
                    return
                }

                if (Build.VERSION.SDK_INT >= 23) {
                    activity.requestPermissions(permissionsList.toTypedArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                }

                return
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return
    }

    fun addPermission(permissionsList: MutableList<String>, permission: String, activity: AppCompatActivity): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!activity.shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    fun validaPermissao(activity: AppCompatActivity): Boolean {

        val permissionsList = ArrayList<String>()
        addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE, activity)
        addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE, activity)
        addPermission(permissionsList, Manifest.permission.CAMERA, activity)

        return (permissionsList.size <= 0)

    }

}